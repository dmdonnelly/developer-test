# Databee Full-stack Developer Test

## Requirements

This solution was tested with the following versions:

* PHP: v5.5.22
* MySQL: v5.6.32

This solution should work with other versions of the above.

## Database Schema

To minimise wasted space, this solution uses a cut-down version of what the database would contain in a real world solution.

The first table, 'venue', contains just a venue ID and name. In a real world solution, this would logically contain additional details about that specific venue.

The second table, 'venue_booking', contains a venue ID, which is linked to the ID used for 'venue', as well as a booking ID, date, and which period of the day it is (1 = Morning, 2 = Afternoon, 3 = Evening). In a real world solution, the booking ID would be linked to one used for a 'booking' table, which would contain more details about the specific booking than just the date, time, and location. In this table, each row MUST be unique.

## Initial Setup

The following steps must be followed to get the frontend ready for use:

1. Create a database (if one does not already exist), and set a username and password for access to it.
2. Open 'connect.php', and change the values stored in '$db_server', '$db_database', '$db_user', and '$db_password' with the server address, database name, username, and password, respectively.
3. Upload all files to a server that has access to the database server address.
4. Perform the 'Data Setup' task below, making sure to have the 'Reset dataset before repopulating?' option selected.

## Data Setup

To better facilitate testing of the frontend, the file 'setdata.php' can be used to populate the database with randomized data, and can also be used to wipe any existing data from the system before re-populating it.

The file is fairly a simple web application, but for ease of understanding, instructions on how to use it are as follows:

* Number of Venues to Add: If the value in this box is higher than 0, that many venues are added into the system. If there are no existing venues, and this number is not greater than 0, an error is returned and operations stop.
* Number of Existing Bookings to Add: If the value in this box is higher than 0, then that many bookings are added into the system. For the purpose of this solution, a venue is considered to be available for a session (Morning, Afternoon, or Evening) on a chosen date unless there is a booking for that venue on that specific date for that particular session. As bookings are randomly created and must be unique, it is possible for less bookings than the chosen amount to be added into the database.
* Apply additional bookings to each venue: If this option is checked, then the chosen number of bookings above are added to every venue in the system. If this option is not checked, then the specific venue for each booking added to the system is randomly selected from all existing venues, including any added during this operation.
* Reset dataset before repopulating: If this option is checked, the tables used for this solution are Dropped if they exist, and then Created if they do not already exist; this level of redundancy is to ensure the tables are not created if a drop operation both fails, and somehow misses the kill clause for a failure. This option can also be used for easy initial setup.

## The Frontend

The frontend solution, 'search.php', is very lightweight and easy to use, and was built to be responsive using bootstrap. 

There are only two fields that need to be selected, details of which are as follows:

* Date: The date to be searched. As per the specification, this date can be any date from 'Today' to 'Three months from today'.
* Location: The specific venue to find availably at. This dropdown only appears on the page if there is more than 1 venue in the system. A search can only be performer either on all venues, or a specific venue.

Upon hitting submit with values selected for the above, the results are displayed below. To improve readability at a glance, all sessions that are unavailable have the text in red.