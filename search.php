<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Databee Test - Find Availability</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
    <link href="jQueryAssets/jquery.ui.core.min.css" rel="stylesheet" type="text/css">
    <link href="jQueryAssets/jquery.ui.theme.min.css" rel="stylesheet" type="text/css">
    <link href="jQueryAssets/jquery.ui.datepicker.min.css" rel="stylesheet" type="text/css">
    <script src="jQueryAssets/jquery-1.11.1.min.js"></script>
    <script src="jQueryAssets/jquery.ui-1.10.4.datepicker.min.js"></script>
	<script type="text/javascript">
		$(function() {
			// Create the datepicker such that only a three month period is selectable
			$( "#searchdate" ).datepicker({ 
				dateFormat: 'dd-mm-yy',
				minDate: 0,
				maxDate: "+3m"
			});
		});
	</script>
	<!-- All SQL connection code contained to a seperate file -->
	<? include("connect.php"); ?>
	</head>
  <body>
    <div class="container mt-2">
      <div class="row">
          <div class="text-center col-md-4 col-12"></div>
		  <div class="text-center col-md-4 col-12">
			  <div class="col-12 card bg-light mx-auto">
              <h3 class="text-center card-header">Find Availability</h3>
              <form class="card-body" action="<?=$PHP_SELF?>" method="post">
                <div class="form-group row">
                  <label for="date" class="col-form-label">Date</label>
                  <div class="input-group">
					<input type="text" class="form-control" id="searchdate" name="searchdate" value="<? echo $_POST['searchdate']; ?>">
                  </div>
                </div>
		<?
		// Display a Venue Location selecter dropdown box if there is more than 1 venue in the data set
		$sql_query = "SELECT * FROM " . $db_venue;
		$result = mysql_query($sql_query);
		if (!$result) {
			echo("<p>Error performing query: " . mysql_error() . "</p>");
			exit();
		}
		$numrows = mysql_num_rows($result);
		if ($numrows > 1):
		?>
				<div class="form-group row">
                  <label for="location" class="col-form-label">Location</label>
                  <select class="form-control" name="location" id="location">
                    <option value="0">Any</option>
		<? while($row=mysql_fetch_array($result)): ?>
                    <option value="<? echo $row['venue_id']; ?>" <? if ($_POST['location'] == $row['venue_id']): ?>selected <? endif; ?></option><? echo $row['venue_name']; ?></option>
		<? endwhile; ?>
                  </select>
                </div>
		<? endif; ?>
				  <input type="submit" name="submit" value="Submit">
              </form>
            </div>
		  </div>
		  <div class="text-center col-md-4 col-12"></div>
      </div>
    </div>
	<!-- The bottom half of the page is only displayed when a search has been performed -->
	<? if(isset($_POST['submit'])): ?>
	<?
		// Dates are stored as Unix timestamps because they are used below is differing formats
		$date = strtotime($_POST['searchdate']);						// The date searched on
		$startdate = strtotime($_POST['searchdate']." -1 day");		// The date before the chosen date
		$enddate = strtotime($_POST['searchdate']." +1 day");			// The date after the chosen date

		// Find all bookings in the three-day range around the selected date
		$sql_query = "SELECT * FROM " . $db_venue_book . " WHERE date BETWEEN '" . date("Y-m-d",$startdate) . "' AND '" . date("Y-m-d",$enddate)."'";
		// If a specific venue was selected, only select bookings for that venue
		if($_POST['location'] > 0) {
			  $sql_query .= " AND venue_id = ".$_POST['location'];
		}
		$result = mysql_query($sql_query);
		if (!$result) {
			echo("<p>Error performing query: " . mysql_error() . "</p>");
			exit();
		}

		while($row=mysql_fetch_array($result)):
			$b_date = $row['date'];
			$b_venue = $row['venue_id'];
			$b_period = $row['period'];

			// The simplest way to store bookings was as a string stored in an array,
			// as the output display is not created dynamically, thus each entry knows
			// it just needs to check to see whether itself is stored in bookings[]
			$bookings[] = $b_venue."/".$b_date."/".$b_period;
		endwhile;
	
		$sql_query = "SELECT * FROM " . $db_venue;
	  	if($_POST['location'] > 0) {
		  $sql_query .= " WHERE venue_id = ".$_POST['location'];
		}
		$result = mysql_query($sql_query);
		if (!$result) {
			echo("<p>Error performing query: " . mysql_error() . "</p>");
			exit();
		}
	  
		// For each venue, display whether any of the 9 date-session combinations are stored in bookings[]
	  	while($row=mysql_fetch_array($result)):
		?>
	<hr>
	  <? $venue = $row['venue_id']; ?>
    <div class="container">
	  <div class="text-center col-12"><h2><? echo $row['venue_name']; ?></h2></div>
      <div class="row">
        <div class="text-center col-md-4 col-12">
          <h3><? echo date("d-m-Y",$startdate); ?></h3>
          <div class="container d-md-table">
			  <div class="d-table-row">
				  <div class="d-md-table-cell">Morning</div>
				  <div class="d-md-table-cell">
					  <? if(in_array($venue."/".date("Y-m-d",$startdate)."/1",$bookings)): ?>
					  <span style="color:red">Unavailable</span>
					  <? else: ?>
					  Available
					  <? endif; ?>
				  </div>
			  </div>
			  <div class="d-table-row">
				  <div class="d-table-cell">Evening</div>
				  <div class="d-md-table-cell">
					  <? if(in_array($venue."/".date("Y-m-d",$startdate)."/2",$bookings)): ?>
					  <span style="color:red">Unavailable</span>
					  <? else: ?>
					  Available
					  <? endif; ?>
				  </div>
			  </div>
			  <div class="d-table-row">
				  <div class="d-table-cell">Night</div>
				  <div class="d-md-table-cell">
					  <? if(in_array($venue."/".date("Y-m-d",$startdate)."/3",$bookings)): ?>
					  <span style="color:red">Unavailable</span>
					  <? else: ?>
					  Available
					  <? endif; ?>
				  </div>
			  </div>
		  </div>
        </div>
        <div class="text-center col-md-4 col-12">
          <h3><? echo date("d-m-Y",$date); ?></h3>
          <div class="container d-md-table">
			  <div class="d-table-row">
				  <div class="d-md-table-cell">Morning</div>
				  <div class="d-md-table-cell">
					  <? if(in_array($venue."/".date("Y-m-d",$date)."/1",$bookings)): ?>
					  <span style="color:red">Unavailable</span>
					  <? else: ?>
					  Available
					  <? endif; ?>
				  </div>
			  </div>
			  <div class="d-table-row">
				  <div class="d-table-cell">Evening</div>
				  <div class="d-md-table-cell">
					  <? if(in_array($venue."/".date("Y-m-d",$date)."/2",$bookings)): ?>
					  <span style="color:red">Unavailable</span>
					  <? else: ?>
					  Available
					  <? endif; ?>
				  </div>
			  </div>
			  <div class="d-table-row">
				  <div class="d-table-cell">Night</div>
				  <div class="d-md-table-cell">
					  <? if(in_array($venue."/".date("Y-m-d",$date)."/3",$bookings)): ?>
					  <span style="color:red">Unavailable</span>
					  <? else: ?>
					  Available
					  <? endif; ?>
				  </div>
			  </div>
		  </div>
        </div>
        <div class="text-center col-md-4 col-12">
          <h3><? echo date("d-m-Y",$enddate); ?></h3>
          <div class="container d-md-table">
			  <div class="d-table-row">
				  <div class="d-md-table-cell">Morning</div>
				  <div class="d-md-table-cell">
					  <? if(in_array($venue."/".date("Y-m-d",$enddate)."/1",$bookings)): ?>
					  <span style="color:red">Unavailable</span>
					  <? else: ?>
					  Available
					  <? endif; ?>
				  </div>
			  </div>
			  <div class="d-table-row">
				  <div class="d-table-cell">Evening</div>
				  <div class="d-md-table-cell">
					  <? if(in_array($venue."/".date("Y-m-d",$enddate)."/2",$bookings)): ?>
					  <span style="color:red">Unavailable</span>
					  <? else: ?>
					  Available
					  <? endif; ?>
				  </div>
			  </div>
			  <div class="d-table-row">
				  <div class="d-table-cell">Night</div>
				  <div class="d-md-table-cell">
					  <? if(in_array($venue."/".date("Y-m-d",$enddate)."/3",$bookings)): ?>
					  <span style="color:red">Unavailable</span>
					  <? else: ?>
					  Available
					  <? endif; ?>
				  </div>
			  </div>
		  </div>
        </div>
      </div>
    </div>
	<? endwhile; ?>
	<? endif; ?>
  </body>
</html>